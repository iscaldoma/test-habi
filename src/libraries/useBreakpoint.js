import React from 'react';

const getWindowDimensions = () => {
  const { innerWidth: width } = window;
  return width;
}

const useBreakpoint = () => {
  const [width, setWidth] = React.useState(getWindowDimensions);

  React.useEffect(() => {
    function handleResize() {
      setWidth(getWindowDimensions);
    }

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  if (width >= 320 && width <= 480) {
    return 'mobile';
  } else if (width >= 481 && width <= 768) {
    return 'tablet';
  } else {
    return 'web';
  }
}

export default useBreakpoint;