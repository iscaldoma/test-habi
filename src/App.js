//Libraries
import React from 'react';

//Components
import PageWrapper from './components/PageWrapper';
import Home from './components/Home';

const App = () => {
  return (
    <PageWrapper>
      <Home />
    </PageWrapper>
  );
}

export default App;
