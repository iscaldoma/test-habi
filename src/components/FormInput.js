import React from "react";

const FormInput = ({ step, value, handleInput }) => {
  const comboRef = React.useRef(null);
  const { fields } = step;

  React.useEffect(() => {
    if (comboRef.current) {
      comboRef.current.value = value;
    }
  }, [value]);

  return (
    <>
      {fields.map((item, index) => {
        switch (item.type) {
          case 'text':
          case 'email':
          case 'number':
            return <input key={ index } type={ item.type } name={ item.name } className='text-input' placeholder={ step.description } value={ value } onChange={ handleInput } />
          case 'combo':
            if (item.min) {
              let options = [];
              for (let i=item.min; i<=item.max; i++) {
                options.push(i);
              }
              return (
                <select key={ index } name={ item.name } className='text-input' onChange={ handleInput } ref={ comboRef }>
                  {options.map(item => {
                    return <option value={ item } key={ item } >{ item }</option>
                  })}
                </select>
              );
            } else {
              return (
                <select key={ index } name={ item.name } className='text-input' onChange={ handleInput } ref={ comboRef }>
                  {item.options.map(item => {
                    return <option value={ item } key={ item } >{ item }</option>
                  })}
                </select>
              );
            }
            case 'checkbox':
              return (
                <div key={ index }>
                  {item.options.map((check, index) => {
                    let checkedItem = value.find(i => i === check)
                    return (
                      <div key={ index }>
                        <input type={ item.type } name={item.name} value={ check } onClick={ handleInput } checked={ checkedItem } />
                        <span>{check}</span>
                      </div>
                    );
                  })}
                </div>
              )
            case 'file':
              return <input key={ index } type={ item.type } name={ item.name } className='text-input' placeholder={ step.description } onChange={ handleInput } />
            default:
              break;
        }
        return <label key={ index }>No se econtro el tipo de dato</label>;
      })}
    </>
  );
}

export default FormInput