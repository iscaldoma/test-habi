//Libraries
import React from 'react';

//Components
import FormInput from './FormInput';

//Context
import { AppDataContext } from '../context/AppDataContext';

const Email = (props) => {
  const { history } = props;
  const context = React.useContext(AppDataContext);
  const { config }  = context;
  const step = config.find(item => item.step === 2);
  const [email, setEmail] = React.useState('');
  const [error, setError] = React.useState('');

  const handleInput = (e) => {
    setEmail(e.target.value);
  }

  const validateEmail = (email) => {
    return String(email)
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
  };

  const handleClickNext = () => {
    if (email === '' || validateEmail(email) === null) {
      setError('Este campo es obligatorio o el correo no es valido');
    } else {
      let userData = JSON.parse(localStorage.getItem('userData'));
      let tmpUserData = userData.find(item => item.step === step.step);
      if (userData) {
        if (tmpUserData) {
          userData[step.step - 1].path = step.path;
          userData[step.step - 1].data = {
            value: email,
          }
        } else {
          let newStep = {
            path: step.path,
            step: step.step,
            data: {
              value: email,
            }
          }
          userData.push(newStep);
        }
      }
      localStorage.setItem('userData', JSON.stringify(userData));
      history.push('/address');
    }
  }

  React.useEffect(() => {
    let userData = JSON.parse(localStorage.getItem('userData'));
    if (userData) {
      let tempStep = userData.find(item => item.step === step.step);
      if (tempStep && tempStep.data) {
        setEmail(tempStep.data.value);
      }
    }
  }, [step]);

  return (
    <div className='form-container'>
      <div className='title'>
        <h2>{step.description}</h2>
      </div>
      <div className='flex-container'>
        <FormInput step={ step } value={ email } handleInput={ handleInput } />
      </div>
      <div className='form-error'>{ error }</div>
      <div className='flex-container align end'>
        <button className='button-primary' onClick={ handleClickNext }>Siguiente</button>
      </div>
    </div>
  );
}

export default Email;