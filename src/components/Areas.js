//Libraries
import React from 'react';

//Components
import FormInput from './FormInput';

//Context
import { AppDataContext } from '../context/AppDataContext';

const Areas = (props) => {
  const { history } = props;
  const context = React.useContext(AppDataContext);
  const { config }  = context;
  const step = config.find(item => item.step === 5);
  const [areas, setAreas] = React.useState([]);
  const [error, setError] = React.useState('');

  const handleInput = (e) => {
    let value = e.target.value;
    let allAreas = areas;
    let index = allAreas.findIndex(item => item === value);
    if (index === -1) {
      allAreas.push(value);
    } else {
      allAreas.splice(index, 1);
    }

    setAreas(allAreas);
  }

  const handleClickNext = () => {
    if (areas.length === 0) {
      setError('Debe seleccional al menos una opción');
    } else {
      let userData = JSON.parse(localStorage.getItem('userData'));
      let tmpUserData = userData.find(item => item.step === step.step);
      if (userData) {
        if (tmpUserData) {
          userData[step.step - 1].path = step.path;
          userData[step.step - 1].data = {
            value: areas,
          }
        } else {
          let newStep = {
            path: step.path,
            step: step.step,
            data: {
              value: areas,
            }
          }
          userData.push(newStep);
        }
      }
      localStorage.setItem('userData', JSON.stringify(userData));
      history.push('/parking');
    }
  }

  React.useEffect(() => {
    let userData = JSON.parse(localStorage.getItem('userData'));
    if (userData) {
      let tempStep = userData.find(item => item.step === step.step);
      if (tempStep && tempStep.data) {
        setAreas(tempStep.data.value);
      }
    }
  }, [step]);

  return (
    <div className='form-container'>
      <div className='title'>
        <h2>{step.description}</h2>
      </div>
      <div className='flex-container'>
        <FormInput step={ step } value={ areas } handleInput={ handleInput } />
      </div>
      <div className='form-error'>{ error }</div>
      <div className='flex-container align end'>
        <button className='button-primary' onClick={ handleClickNext }>Siguiente</button>
      </div>
    </div>
  );
}

export default Areas;