//Libraries
import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import useBreakpoint from '../libraries/useBreakpoint'

//Comoponents
import BasicData from './BasicData';
import Resume from './Resume';
import Email from './Email';
import Address from './Address';
import Floor from './Floor';
import Areas from './Areas';
import Amount from './Amount';
import Parking from './Parking';
import Picture from './Picture';
import Elevator from './Elevator';

//Context
import { AppDataContext } from '../context/AppDataContext';

//Css
import '../App.scss';

const Home = () => {
  const context = React.useContext(AppDataContext);
  const breakpoint = useBreakpoint();
  const history = require('history').createBrowserHistory();
  const [showModal, setShowModal] = React.useState(false);

  const componentsMap = {
    'basic-data': BasicData,
    'email': Email,
    'address': Address,
    'floor': Floor,
    'areas': Areas,
    'parking': Parking,
    'amount': Amount,
    'picture': Picture,
    'elevator': Elevator,
  }

  const handleResumeButton = () => {
    setShowModal(!showModal);
  }

  const dinamycRoutes = (config) => {
    var routeJSX = config.map((item) => {
      return <Route exact key={ item.step } path={ item.path } component={  componentsMap[item.component] } />
    });
    return routeJSX;
  }

  React.useEffect(() => {
    let storage = localStorage.getItem('userData');
    if (storage === null) {
      let userData = [{ step: 1, data: null }];
      localStorage.setItem('userData', JSON.stringify(userData));
    }
  }, []);

  var routes = dinamycRoutes(context.config);

  return (
    <div className='landing-wrapper flex-container'>
      <div className='landing-content flex-container justify align'>
        <Router history={history}>
          <Switch>
            { routes }
          </Switch>
        </Router>
      </div>

      {breakpoint === 'web' &&
        <div className='landing-resume'>
          <Resume history={history} />
        </div>
      }

      {(breakpoint === 'mobile' || breakpoint === 'tablet') &&
        <div className='landing-button-container'>
          <button className='button-primary' onClick={ handleResumeButton }>Resumen</button>
        </div>
      }

      {showModal &&
        <div className='landing-modal flex-container justify align'>
          <div className='landing-modal-content'>
            <div className='flex-container align end'>
              <div onClick={ handleResumeButton }>X</div>
            </div>
            <div>
              <Resume history={history} />
            </div>
          </div>
        </div>
      }
    </div>
  );
}

export default Home;