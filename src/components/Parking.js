//Libraries
import React from 'react';

//Components
import FormInput from './FormInput';

//Context
import { AppDataContext } from '../context/AppDataContext';

const Parking = (props) => {
  const { history } = props;
  const context = React.useContext(AppDataContext);
  const { config }  = context;
  const step = config.find(item => item.step === 6);
  const [parking, setParking] = React.useState('Si');
  const [error, setError] = React.useState('');

  const handleInput = (e) => {
    setParking(e.target.value);
  }

  const handleClickNext = () => {
    if (parking === '') {
      setError('Este campo es obligatorio');
    } else {
      let userData = JSON.parse(localStorage.getItem('userData'));
      let tmpUserData = userData.find(item => item.step === step.step);
      if (userData) {
        if (tmpUserData) {
          userData[step.step - 1].path = step.path;
          userData[step.step - 1].data = {
            value: parking,
          }
        } else {
          let newStep = {
            path: step.path,
            step: step.step,
            data: {
              value: parking,
            }
          }
          userData.push(newStep);
        }
      }
      localStorage.setItem('userData', JSON.stringify(userData));
      history.push('/amount');
    }
  }

  React.useEffect(() => {
    let userData = JSON.parse(localStorage.getItem('userData'));
    if (userData) {
      let tempStep = userData.find(item => item.step === step.step);
      if (tempStep && tempStep.data) {
        setParking(tempStep.data.value);
      }
    }
  }, [step]);

  return (
    <div className='form-container'>
      <div className='title'>
        <h2>{step.description}</h2>
      </div>
      <div className='flex-container'>
        <FormInput step={ step } value={ parking } handleInput={ handleInput } />
      </div>
      <div className='form-error'>{ error }</div>
      <div className='flex-container align end'>
        <button className='button-primary' onClick={ handleClickNext }>Siguiente</button>
      </div>
    </div>
  );
}

export default Parking;