//Libraries
import React from 'react';

//Components
import FormInput from './FormInput';

//Context
import { AppDataContext } from '../context/AppDataContext';

const Picture = (props) => {
  const { history } = props;
  const context = React.useContext(AppDataContext);
  const { config }  = context;
  const step = config.find(item => item.step === 8);
  const [picture, setPicture] = React.useState('');

  const handleInput = (e) => {
    setPicture(e.target.value);
  }

  const handleClickNext = () => {
    let userData = JSON.parse(localStorage.getItem('userData'));
    let tmpUserData = userData.find(item => item.step === step.step);
    if (userData) {
      if (tmpUserData) {
        userData[step.step - 1].path = step.path;
        userData[step.step - 1].data = {
          value: picture,
        }
      } else {
        let newStep = {
          path: step.path,
          step: step.step,
          data: {
            value: picture,
          }
        }
        userData.push(newStep);
      }
    }
    localStorage.setItem('userData', JSON.stringify(userData));
    history.push('/elevator');
  }

  React.useEffect(() => {
    let userData = JSON.parse(localStorage.getItem('userData'));
    if (userData) {
      let tempStep = userData.find(item => item.step === step.step);
      if (tempStep && tempStep.data) {
        setPicture(tempStep.data.value);
      }
    }
  }, [step]);

  return (
    <div className='form-container'>
      <div className='title'>
        <h2>{step.description}</h2>
      </div>
      <div className='flex-container'>
        <FormInput step={ step } value={ picture } handleInput={ handleInput } />
      </div>
      <div>
        {picture !== '' &&
          <div>Archivo Seleccionado: <strong>{ picture }</strong></div>
        }
      </div>
      <div className='flex-container align end'>
        <button className='button-primary' onClick={ handleClickNext }>Siguiente</button>
      </div>
    </div>
  );
}

export default Picture;