//Libraries
import React from 'react';

//Context
import { AppDataContext } from '../context/AppDataContext';

const Resume = (props) => {
  const { history } = props;
  const context = React.useContext(AppDataContext);
  const { config } = context;
  const userData = JSON.parse(localStorage.getItem('userData'));
  const [path, setPath] = React.useState(history.location.pathname);

  React.useEffect(() => {
    history.listen((location) => {
      setPath(location.pathname);
    });
  },[history]);

  return (
    <div>
      <h3>Resumen</h3>
      <ul>
        {config.map((item) => {
          const enabled = userData && userData.find(element => element.step === item.step);
          const active = item.path === path ? true : false;

          return (
            <li className={`resume-item ${enabled ? 'enabled' : ''} ${active ? 'active' : ''}`} key={item.step}>
              <a href={ item.path }>
                <div>Paso {item.step}</div>
                <div>{item.title}</div>
              </a>
            </li>
          );
        })}
      </ul>
    </div>
  );
}

export default Resume;