// Libraries
import React from 'react';

//Components
import FormInput from './FormInput';

//Context
import { AppDataContext } from '../context/AppDataContext';

const BasicData = (props) => {
  const { history } = props;
  const context = React.useContext(AppDataContext);
  const userData = JSON.parse(localStorage.getItem('userData'));
  const { config }  = context;
  const step = config.find(item => item.step === 1);
  const [name, setName] = React.useState('');
  const [error, setError] = React.useState('');

  const useQuery = () => {
    const { search } = history.location;
    return React.useMemo(() => new URLSearchParams(search), [search]);
  }

  let query = useQuery();
  const isEndSteps = query.get('end');

  const handleInput = (e) => {
    setName(e.target.value);
  }

  const handleClickNext = () => {
    if (name === '') {
      setError('Este campo es obligatorio');
    } else {
      if (userData) {
        userData[step.step - 1].path = step.path;
        userData[step.step - 1].data = {
          value: name,
        }
      }
      localStorage.setItem('userData', JSON.stringify(userData));
      history.push('/email');
    }
  }

  const resetData = () => {
    let userData = [{ step: 1, data: null }];
    localStorage.setItem('userData', JSON.stringify(userData));
    window.location = '/';
  }

  React.useEffect(() => {
    if (userData) {
      let tempStep = userData.find(item => item.step === step.step);
      if (tempStep && tempStep.data) {
        setName(tempStep.data.value);
      }
    }
  }, [step, userData]);

  return (
    <div className='form-container'>
      {isEndSteps === null ?
        <>
          <div className='title'>
            <h2>{step.description}</h2>
          </div>
          <div className='flex-container'>
            <FormInput step={ step } value={ name } handleInput={ handleInput } />
          </div>
          <div className='form-error'>{ error }</div>
          <div className='flex-container align end'>
            <button className='button-primary' onClick={ handleClickNext }>Siguiente</button>
          </div>
        </>
      :
        <div className='resume-wrapper'>
          <div className='title'>
            <h2>Resumen general</h2>
          </div>
          { userData && userData.map(item => {
            let stepInfo = config.find(conf => conf.step === item.step);
            return (
              <div key={ item.step } className='flex-container align between' style={{margin: '1rem 0'}}>
                <strong>Paso {item.step} [{stepInfo.title}]:</strong>
                <div>{item.data.value}</div>
              </div>
            );
          })}
          <div className='flex-container align justify'>
            <button className='button-primary' onClick={ resetData }>Borrar todo y empezar de nuevo</button>
          </div>
        </div>
      }
    </div>
  );
}

export default BasicData;