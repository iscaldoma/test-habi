//Libraries
import React from 'react';

//Components
import FormInput from './FormInput';

//Context
import { AppDataContext } from '../context/AppDataContext';

const Elevator = (props) => {
  const { history } = props;
  const context = React.useContext(AppDataContext);
  const { config }  = context;
  const step = config.find(item => item.step === 9);
  const [elevator, setElevator] = React.useState('Si');
  const [error, setError] = React.useState('');

  const handleInput = (e) => {
    setElevator(e.target.value);
  }

  const handleClickNext = () => {
    if (elevator === '') {
      setError('Este campo es obligatorio');
    } else {
      let userData = JSON.parse(localStorage.getItem('userData'));
      let tmpUserData = userData.find(item => item.step === step.step);
      if (userData) {
        if (tmpUserData) {
          userData[step.step - 1].path = step.path;
          userData[step.step - 1].data = {
            value: elevator,
          }
        } else {
          let newStep = {
            path: step.path,
            step: step.step,
            data: {
              value: elevator,
            }
          }
          userData.push(newStep);
        }
      }
      localStorage.setItem('userData', JSON.stringify(userData));
      history.push('/?end=true');
    }
  }

  React.useEffect(() => {
    let userData = JSON.parse(localStorage.getItem('userData'));
    if (userData) {
      let tempStep = userData.find(item => item.step === step.step);
      if (tempStep && tempStep.data) {
        setElevator(tempStep.data.value);
      }
    }
  }, [step]);

  return (
    <div className='form-container'>
      <div className='title'>
        <h2>{step.description}</h2>
      </div>
      <div className='flex-container'>
        <FormInput step={ step } value={ elevator } handleInput={ handleInput } />
      </div>
      <div className='form-error'>{ error }</div>
      <div className='flex-container align end'>
        <button className='button-primary' onClick={ handleClickNext }>Finalizar</button>
      </div>
    </div>
  );
}

export default Elevator;