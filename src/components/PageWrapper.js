//Libraries
import React from "react";

//Context
import { AppDataContext } from "../context/AppDataContext";

const PageWrapper = (props) => {
  const config = [
    {
      step: 1,
      component: 'basic-data',
      path: '/',
      title: 'Información General',
      description: 'Nombre Completo',
      fields: [
        {
          type: 'text',
          name: 'nombre',
          required: true,
        }
      ],
    },
    {
      step: 2,
      component: 'email',
      path: '/email',
      title: 'Correo Electrónico',
      description: 'Ingrese su correo electronico',
      fields: [
        {
          type: 'email',
          name: 'email',
          required: true,
        }
      ],
    },
    {
      step: 3,
      component: 'address',
      path: '/address',
      title: 'Dirección',
      description: 'Escriba su dirección completa',
      fields: [
        {
          type: 'text',
          name: 'address',
          required: true,
        }
      ],
    },
    {
      step: 4,
      component: 'floor',
      path: '/floor',
      title: 'Número de piso',
      description: 'Eliga un numero de piso de su vivienda',
      fields: [
        {
          type: 'combo',
          name: 'floor',
          min: 1,
          max: 50,
          required: true,
        }
      ],
    },{
      step: 5,
      component: 'areas',
      path: '/areas',
      title: 'Áreas comunes',
      description: 'Seleccione cuál de estas áreas tiene tu vivienda',
      fields: [
        {
          type: 'checkbox',
          options: ['Zona BBQ', 'Salón comunal', 'Parque de juegos'],
          required: false,
        }
      ],
    },
    {
      step: 6,
      component: 'parking',
      path: '/parking',
      title: 'Estacionamiento',
      description: '¿Tiene estacionamiento?',
      fields: [
        {
          type: 'combo',
          name: 'floor',
          options: ['Si', 'No'],
          required: true,
        }
      ],
    },
    {
      step: 7,
      component: 'amount',
      path: '/amount',
      title: 'Monto de venta',
      description: 'Monto de venta',
      fields: [
        {
          type: 'number',
          name: 'amount',
          required: true,
        }
      ],
    },
    {
      step: 8,
      component: 'picture',
      path: '/picture',
      title: 'Imágen',
      description: 'Seleccione una imágen',
      fields: [
        {
          type: 'file',
          name: 'file',
          required: false,
        }
      ],
    },
    {
      step: 9,
      component: 'elevator',
      path: '/elevator',
      title: 'Elevador',
      description: 'Tiene Elevador?',
      fields: [
        {
          type: 'combo',
          name: 'elevator',
          options: ['Si', 'No'],
          required: true,
        }
      ],
    }
  ];

  return (
    <AppDataContext.Provider
      value = {{
        config: config,
      }}
    >
      {props.children}
    </AppDataContext.Provider>
  );
}

export default PageWrapper;